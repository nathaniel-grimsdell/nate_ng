import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-picks',
  templateUrl: './picks.component.html',
  styleUrls: ['./picks.component.css']
})
export class PicksComponent implements OnInit {

  goBack(): void {
    this.router.navigate(['/pools']);
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
