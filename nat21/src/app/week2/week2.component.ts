import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-week2',
  templateUrl: './week2.component.html',
  styleUrls: ['./week2.component.css']
})
export class Week2Component implements OnInit {

  goBack(): void {
    this.router.navigate(['/pools']);
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
