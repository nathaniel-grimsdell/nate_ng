/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SunwolvesComponent } from './sunwolves.component';

describe('SunwolvesComponent', () => {
  let component: SunwolvesComponent;
  let fixture: ComponentFixture<SunwolvesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SunwolvesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SunwolvesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
