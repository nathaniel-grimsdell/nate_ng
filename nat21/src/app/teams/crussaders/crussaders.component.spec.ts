/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CrussadersComponent } from './crussaders.component';

describe('CrussadersComponent', () => {
  let component: CrussadersComponent;
  let fixture: ComponentFixture<CrussadersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrussadersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrussadersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
