/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { BluebullsComponent } from './bluebulls.component';

describe('BluebullsComponent', () => {
  let component: BluebullsComponent;
  let fixture: ComponentFixture<BluebullsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluebullsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BluebullsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
