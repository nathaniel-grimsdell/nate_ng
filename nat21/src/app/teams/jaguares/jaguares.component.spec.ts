/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { JaguaresComponent } from './jaguares.component';

describe('JaguaresComponent', () => {
  let component: JaguaresComponent;
  let fixture: ComponentFixture<JaguaresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JaguaresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JaguaresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
