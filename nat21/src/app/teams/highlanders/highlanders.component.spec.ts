/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HighlandersComponent } from './highlanders.component';

describe('HighlandersComponent', () => {
  let component: HighlandersComponent;
  let fixture: ComponentFixture<HighlandersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighlandersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighlandersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
