/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { StommersComponent } from './stommers.component';

describe('StommersComponent', () => {
  let component: StommersComponent;
  let fixture: ComponentFixture<StommersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StommersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StommersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
