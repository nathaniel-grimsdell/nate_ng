/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { RebbelsComponent } from './rebbels.component';

describe('RebbelsComponent', () => {
  let component: RebbelsComponent;
  let fixture: ComponentFixture<RebbelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RebbelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RebbelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
