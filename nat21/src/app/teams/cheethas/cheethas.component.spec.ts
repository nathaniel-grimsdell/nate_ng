/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CheethasComponent } from './cheethas.component';

describe('CheethasComponent', () => {
  let component: CheethasComponent;
  let fixture: ComponentFixture<CheethasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheethasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheethasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
