import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {CalculatorComponent} from './calculator/calculator.component';
import {CountryListComponent} from './country-list/country-list.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import { BluebullsComponent } from './teams/bluebulls/bluebulls.component';
import { LionsComponent } from './teams/lions/lions.component';
import { ChiefsComponent } from './teams/chiefs/chiefs.component';
import { CheethasComponent } from './teams/cheethas/cheethas.component';
import { SharksComponent } from './teams/sharks/sharks.component';
import { HighlandersComponent } from './teams/highlanders/highlanders.component';
import { CrussadersComponent } from './teams/crussaders/crussaders.component';
import { RebbelsComponent } from './teams/rebbels/rebbels.component';
import { StommersComponent } from './teams/stommers/stommers.component';
import { SunwolvesComponent } from './teams/sunwolves/sunwolves.component';
import { BluesComponent } from './teams/blues/blues.component';
import { JaguaresComponent } from './teams/jaguares/jaguares.component';
import {ProfileComponent} from './profile/profile.component';
import {PicksComponent} from './picks/picks.component';
import {PoolsComponent} from './pools/pools.component';
import {Week2Component} from './week2/week2.component'


const
  ROUTES: Routes = [

    {
      path: 'calculator', component: CalculatorComponent,
    },
    {
      path: 'country-list', component: CountryListComponent,
    },
    {
      path: 'dashboard', component: DashboardComponent,
      data: [{
        pageHeader: 'Nate_NG Dashboard!',
      }]
    },
    {
      path: 'profile', component: ProfileComponent,
    },
    {
      path: 'picks', component: PicksComponent,
    },{
      path: 'pools', component: PoolsComponent,
    },{
      path: 'week2', component: Week2Component,
    },






    {
      path: 'bluebulls', component: BluebullsComponent,
    },
    {
      path: 'lions', component: LionsComponent,
    },
    {
      path: 'chiefs', component: ChiefsComponent,
    },
    {
      path: 'cheethas', component: CheethasComponent,
    },
    {
      path: 'sharks', component: SharksComponent,
    },
    {
      path: 'highlanders', component: HighlandersComponent,
    },
    {
      path: 'crussaders', component: CrussadersComponent,
    },
    {
      path: 'rebbels', component: RebbelsComponent,
    },
    {
      path: 'stommers', component: StommersComponent,
    },
    {
      path: 'sunwolves', component: SunwolvesComponent,
    },
    {
      path: 'blues', component: BluesComponent,
    },
    {
      path: 'jaguares', component: JaguaresComponent,
    },
    // {
    //   path: '', component: Component,
    //   data: [{
    //     pageHeader: 'Nate_NG ...',
    //   }]
    // },
    // {
    //   path: '', component: Component,
    //   data: [{
    //     pageHeader: 'Nate_NG ...',
    //   }]
    // },
    // {
    //   path: '', component: Component,
    //   data: [{
    //     pageHeader: 'Nate_NG ...',
    //   }]
    // },
    // {
    //   path: '', component: Component,
    //   data: [{
    //     pageHeader: 'Nate_NG ...',
    //   }]
    // },

  ];

export const routing = RouterModule.forRoot(ROUTES);
@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES)
  ],
  exports: [
    RouterModule
  ],
  providers: [

  ]
})
export class AppRoutingModule {
}
