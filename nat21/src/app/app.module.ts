import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {routing} from './app.routing';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { CountryListComponent } from './country-list/country-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BluebullsComponent } from './teams/bluebulls/bluebulls.component';
import { LionsComponent } from './teams/lions/lions.component';
import { ChiefsComponent } from './teams/chiefs/chiefs.component';
import { CheethasComponent } from './teams/cheethas/cheethas.component';
import { SharksComponent } from './teams/sharks/sharks.component';
import { HighlandersComponent } from './teams/highlanders/highlanders.component';
import { CrussadersComponent } from './teams/crussaders/crussaders.component';
import { RebbelsComponent } from './teams/rebbels/rebbels.component';
import { StommersComponent } from './teams/stommers/stommers.component';
import { SunwolvesComponent } from './teams/sunwolves/sunwolves.component';
import { BluesComponent } from './teams/blues/blues.component';
import { JaguaresComponent } from './teams/jaguares/jaguares.component';
import { ProfileComponent } from './profile/profile.component';
import { PicksComponent } from './picks/picks.component';
import { PoolsComponent } from './pools/pools.component';
import { Week2Component } from './week2/week2.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    CalculatorComponent,
    CountryListComponent,
    DashboardComponent,
    BluebullsComponent,
    LionsComponent,
    ChiefsComponent,
    CheethasComponent,
    SharksComponent,
    ChiefsComponent,
    HighlandersComponent,
    CrussadersComponent,
    RebbelsComponent,
    StommersComponent,
    SunwolvesComponent,
    BluesComponent,
    JaguaresComponent,
    ProfileComponent,
    PicksComponent,
    PoolsComponent,
    Week2Component,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
