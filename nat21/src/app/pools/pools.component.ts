import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-pools',
  templateUrl: './pools.component.html',
  styleUrls: ['./pools.component.css']
})
export class PoolsComponent implements OnInit {

  goBack(): void {
    this.router.navigate(['/picks']);
  }

  goThere(): void {
    this.router.navigate(['/week2']);
  }

  constructor(private router: Router,) { }

  ngOnInit() {
  }


}
