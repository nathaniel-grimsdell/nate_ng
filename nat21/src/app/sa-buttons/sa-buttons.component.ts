import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-sa-buttons',
  templateUrl: './sa-buttons.component.html',
  styleUrls: ['./sa-buttons.component.css']
})
export class SaButtonsComponent implements OnInit {
  @Input() type: string;
  @Input() icon: string;
  @Input() tooltip: string;
  @Output() private onClick: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  click() {
    this.onClick.emit();
  }
}
