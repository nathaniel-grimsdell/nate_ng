import { Nat21Page } from './app.po';

describe('nat21 App', function() {
  let page: Nat21Page;

  beforeEach(() => {
    page = new Nat21Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
